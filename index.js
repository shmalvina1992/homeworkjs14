"use strict"

const changeButton = document.querySelector('.change-button');

function usedarkmode () {
	const body = document.body;
	const wasDarkMode = localStorage.getItem('darkmode') === 'true';

	localStorage.setItem('darkmode', !wasDarkMode);
	body.classList.toggle('dark-mode', !wasDarkMode);
	
}

document.querySelector('.change-button').addEventListener('click', usedarkmode);

function stopUpdate () {
	document.body.classList.toggle('dark-mode', localStorage.getItem('darkmode') === 'true');
}

document.addEventListener('DOMContentLoaded', stopUpdate);